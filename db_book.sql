-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 11 fév. 2018 à 17:02
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_book`
--

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE `item` (
  `type_vdr` varchar(25) NOT NULL,
  `book_site_id` varchar(255) NOT NULL,
  `annee` varchar(255) DEFAULT NULL,
  `auteur` varchar(255) DEFAULT NULL,
  `conditions` varchar(255) DEFAULT NULL,
  `date_ajout` tinyblob,
  `description` varchar(255) DEFAULT NULL,
  `editeur` varchar(255) DEFAULT NULL,
  `illustrateur` varchar(255) DEFAULT NULL,
  `mot_cle` varchar(255) DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `prix` bigint(20) NOT NULL,
  `quantite` int(11) NOT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `url_pic` varchar(255) DEFAULT NULL,
  `binding` varchar(255) DEFAULT NULL,
  `condition_note` varchar(255) DEFAULT NULL,
  `edition` varchar(255) DEFAULT NULL,
  `expedited_shipping` varchar(255) DEFAULT NULL,
  `external_product_id` varchar(255) DEFAULT NULL,
  `external_product_id_type` varchar(255) DEFAULT NULL,
  `langage` varchar(255) DEFAULT NULL,
  `merchant_shipping_group_name` varchar(255) DEFAULT NULL,
  `volume` varchar(255) DEFAULT NULL,
  `will_ship_internationally` varchar(255) DEFAULT NULL,
  `book_type` varchar(255) DEFAULT NULL,
  `bookseller_catalogue` varchar(255) DEFAULT NULL,
  `dust_jacket` bit(1) DEFAULT NULL,
  `inscription_type` varchar(255) DEFAULT NULL,
  `jacket_condition` varchar(255) DEFAULT NULL,
  `producttype` varchar(255) DEFAULT NULL,
  `publish_place` varchar(255) DEFAULT NULL,
  `signed` bit(1) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`type_vdr`, `book_site_id`, `annee`, `auteur`, `conditions`, `date_ajout`, `description`, `editeur`, `illustrateur`, `mot_cle`, `observation`, `prix`, `quantite`, `sku`, `titre`, `url_pic`, `binding`, `condition_note`, `edition`, `expedited_shipping`, `external_product_id`, `external_product_id_type`, `langage`, `merchant_shipping_group_name`, `volume`, `will_ship_internationally`, `book_type`, `bookseller_catalogue`, `dust_jacket`, `inscription_type`, `jacket_condition`, `producttype`, `publish_place`, `signed`, `size`, `subject`) VALUES
('AmazBook', '', '1922', 'moi', NULL, 0x323031382d30312d3238, 'Ceci est mon livre', 'forest', 'tarral', 'classique', 'bon livre', 40, 1, '556', 'Ma vie mon oeuvre', 'nicolas.jpg', 'hard cover', 'bords frottes', 'dunod', 'boxed', NULL, 'nombre', NULL, 'commercant', '1', 'yes', '', '', NULL, '', '', '', '', NULL, '', ''),
('AmazBook', '301234', '1902', 'yoyo', 'neuf', 0xaced00057372000d6a6176612e74696d652e536572955d84ba1b2248b20c00007870770703000007e2010d78, 'un gros bouquin', 'toto', 'beber', 'classique', 'pourrave', 50, 1, '1234', 'Guerre et paix', 'nicolas.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('AmazBook', '301235', '1993', 'robert reich', 'comme neuf', 0xaced00057372000d6a6176612e74696d652e536572955d84ba1b2248b20c00007870770703000007e2010d78, 'livre deconomie', 'dunod', '', 'economie', 'bonne facture', 35, 1, '5555', 'economie mondialisée', '', 'hard cover', 'bords frottés', 'dunod', 'boxed', '18', 'nombre', 'francais', 'commercant', '1', 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Abebook', '301236', '1993', 'robert reich', 'comme neuf', 0xaced00057372000d6a6176612e74696d652e536572955d84ba1b2248b20c00007870770703000007e2010d78, 'livre d\'economie', 'dunod', '', 'economie', 'Bonne facture', 35, 1, '5555', 'economie mondialisée', '', 'dure', NULL, 'Dunod', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'broché', 'catalogue neuf', b'1', 'inscription', 'Bonne', 'livre', 'france', b'1', '19x23', 'eco'),
('Abebook', '301237', '1926', 'daudet', 'good', 0xaced00057372000d6a6176612e74696d652e536572955d84ba1b2248b20c00007870770703000007e2010d78, 'Grand classique', 'le moulin', 'pagnol', 'desesperer', 'observation', 20, 1, '7778', 'tartarin de tarascon', 'nicolas.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL, b'0', NULL, NULL),
('Abebook', '5000abe', '2003', 'Qnthony Robbins', ';oyen', NULL, 'self help Livre', 'robert lqfont', '', 'self-help', 'abimé sur les coins', 15, 1, '5000', 'Pouvoir illi;it2', NULL, ' BathBook ,dure', NULL, 'Reponses,robert lafon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'livre', '', b'1', 'non', 'en mauvais état', 'livre', 'france', b'0', '25*45', 'aide personnelle'),
('AmazBook', '5000amz', '2003', 'Qnthony Robbins', ';oyen', NULL, 'self help Livre', 'robert lqfont', '', 'self-help', 'abimé sur les coins', 15, 1, '5000', 'Pouvoir illi;it2', NULL, ' BathBook ,dure', ' D\'occasionBonétat ', 'Reponses,robert lafon', NULL, 'robert lqfont', 'EAN', ' Français ', 'la poste', '', ' 15 , 15 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('AmazBook', '56000', '1922', 'moi', NULL, 0x323031382d30312d3238, 'Ceci est mon livre', 'forest', 'tarral', 'classique', 'bon livre', 40, 1, '556', 'Ma vie mon oeuvre', 'nicolas.jpg', 'hard cover', 'bords frottes', 'dunod', 'boxed', NULL, 'nombre', NULL, 'commercant', '1', 'yes', '', '', NULL, '', '', '', '', NULL, '', ''),
('Abebook', '56001', '1922', 'moi', NULL, 0x323031382d30312d3238, 'Ceci est mon livre', 'forest', 'tarral', 'classique', 'bon livre', 40, 1, '556', 'Ma vie mon oeuvre', 'nicolas.jpg', '', '', '', '', NULL, '', NULL, '', '', '', 'broche', 'catalogue neuf', NULL, 'inscription', 'bonne', 'livre', 'france', NULL, '19x23', 'eco');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`book_site_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
