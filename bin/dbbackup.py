import os
import time
import datetime
import pymysql

def main():

    DB_HOST = 'localhost'
    DB_USER = 'root'
    DB_USER_PASSWORD = ''
    DB_NAME = 'db_book'
    BACKUP_PATH = 'F:\\CODE\\PYTHON\\bellportal\\bin'


    DATETIME = time.strftime('%m%d%Y-%H%M%S')
    TODAYBACKUPPATH = BACKUP_PATH + DATETIME

    os.makedirs(TODAYBACKUPPATH, mode=0755)

    dumpcmd = "mysqldump -u " + DB_USER + " -p" + DB_USER_PASSWORD + " " + DB_NAME + " > " + TODAYBACKUPPATH + "/" + DB_NAME + ".sql"
    os.system(dumpcmd)

    return DATETIME;

def update():
    return  os.listdir('F:\\CODE\\PYTHON\\bellportal\\bin')

def get_10():
    con = pymysql.connect('127.0.0.1', 'root', '', 'db_book');

    with con:

        cur = con.cursor()
        cur.execute("""SELECT * from item ORDER BY book_site_id DESC LIMIT 10""")
        rows = cur.fetchall()
        cur.close()
        con.commit()
        return rows
