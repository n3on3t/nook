import web
from web import form
import os
from mymodels import Item
import datetime

render = web.template.render('templates/', base="layout")

urls = ('/items', 'display_items', '/addBook', 'add_book',
        '/', 'index',
        '/delBook', 'delete_book')


app = web.application(urls, globals())


if web.config.get('_session') is None:
    store = web.session.DiskStore('sessions')
   ## session = web.session.Session(app, store,
     ## initializer={'user': None})
    session = web.session.Session(app, store,
                                  initializer={'login' : 0, 'privilege' :0})
    web.config._session = session
else:
    session = web.config._session

    myform = form.Form(
        form.Textbox("Sku", form.notnull))

    loopform = form.Form(
        form.Textbox("Username", form.notnull),
        form.Password("Password", form.notnull),

        # loop_add = form.Form(
        form.Textarea("address", form.notnull, row=60, cols=80, ))

    ipform = form.Form(
        form.Textbox("ip", form.notnull))

class index(object):
    def GET(self):
        start = ["/addBook", "/delBook"]
	return render.start(start)


class add_book(object):
    def GET(self):
        data = "Ajout de livre."
        form = myform()

        return render.form_bell(form, data)

    def POST(self):
        grandma = Item.create(type_vdr='AmazBook', book_site='56000', sku='556', titre='Ma vie mon oeuvre',
                              editeur='forest', description='Ceci est mon livre', auteur='moi', annee='1922',
                              prix='40', condition='neuf', illustrateur='tarral', mot_cle='classique',
                              url_pic='nicolas.jpg',
                              date_ajout=datetime.date.today(), observation='bon livre', quantite='1',
                              external_product_id='18', external_product_id_type='nombre', binding='hard cover',
                              edition='dunod', condition_note='bords frottes',
                              expedited_shipping='boxed', will_ship_internationally='yes',
                              merchant_shipping_group_name='commercant', language='francais',
                              volume='1', subject='', dust_jacket='', bookseller_catalogue='', size='',
                              jacket_condition='',
                              book_type='', publish_place='', inscription_type='', producttype='', signed=''
                              )

        grandmaAbe = Item.create(type_vdr='Abebook', book_site='56001', sku='556', titre='Ma vie mon oeuvre',
                                 editeur='forest', description='Ceci est mon livre', auteur='moi', annee='1922',
                                 prix='40', condition='neuf', illustrateur='tarral', mot_cle='classique',
                                 url_pic='nicolas.jpg',
                                 date_ajout=datetime.date.today(), observation='bon livre', quantite='1',
                                 external_product_id='', external_product_id_type='', binding='', edition='',
                                 condition_note='',
                                 expedited_shipping='', will_ship_internationally='', merchant_shipping_group_name='',
                                 language='',
                                 volume='', subject='eco', dust_jacket='', bookseller_catalogue='catalogue neuf',
                                 size='19x23', jacket_condition='bonne',
                                 book_type='broche', publish_place='france', inscription_type='inscription',
                                 producttype='livre', signed=False
                                 )
        grandma.save()
        grandmaAbe.save()
        return render.form_ok(datetime.date.today())

class display_items(object):
    def GET(self):
        date = 0
        #launch = dbbackup
        #book = Item.get(Item.annee == '1902')
        #print book.auteur
        #data = []
        #data = launch.get_10()
        print "file create %s\n" % date
        return render.form_ok(date)


class delete_book(object):
    def GET(self):
        data = "Supprimer livre."
        form = myform()
        return render.form_delete(form, data)


    def POST(self):
        form = myform()
        if not form.validates():
            return render.delLivre(form)
        else:
            sku = form['phone_numbers'].value
            print sku




if __name__ == "__main__":
    web.internalerror = web.debugerror
    app.run()
