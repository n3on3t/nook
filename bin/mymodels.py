from peewee import *

database = MySQLDatabase('db_book', **{'user': 'root'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Item(BaseModel):
    annee = CharField(null=True)
    auteur = CharField(null=True)
    binding = CharField(null=True)
    book_site = CharField(db_column='book_site_id', primary_key=True)
    book_type = CharField(null=True)
    bookseller_catalogue = CharField(null=True)
    condition_note = CharField(null=True)
    conditions = CharField(null=True)
    date_ajout = TextField(null=True)
    description = CharField(null=True)
    dust_jacket = UnknownField(null=True)  # bit
    editeur = CharField(null=True)
    edition = CharField(null=True)
    expedited_shipping = CharField(null=True)
    external_product = CharField(db_column='external_product_id', null=True)
    external_product_id_type = CharField(null=True)
    illustrateur = CharField(null=True)
    inscription_type = CharField(null=True)
    jacket_condition = CharField(null=True)
    langage = CharField(null=True)
    merchant_shipping_group_name = CharField(null=True)
    mot_cle = CharField(null=True)
    observation = CharField(null=True)
    prix = BigIntegerField()
    producttype = CharField(null=True)
    publish_place = CharField(null=True)
    quantite = IntegerField()
    signed = UnknownField(null=True)  # bit
    size = CharField(null=True)
    sku = CharField(null=True)
    subject = CharField(null=True)
    titre = CharField(null=True)
    type_vdr = CharField()
    url_pic = CharField(null=True)
    volume = CharField(null=True)
    will_ship_internationally = CharField(null=True)

    class Meta:
        db_table = 'item'

